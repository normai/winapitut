﻿/**
 *  file       : 20220102°1313 winapitut/d01_simple_window/d01_simple_window.c
 *  summary    : Open a plain window without any functionality
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>

const char g_szClassName[] = "myWindowClass";

// Step 4 — The Window Procedure
LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_CLOSE:
         DestroyWindow(hwnd);
         break;
      case WM_DESTROY:
         PostQuitMessage(0);
         break;
      default:
         return DefWindowProc(hwnd, msg, wParam, lParam);
   }
   return 0;
}

// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInstance
                    , _In_opt_ HINSTANCE hPrevInstance
                     , _In_ LPSTR lpCmdLine
                      , _In_ int nCmdShow
                       )
{
   WNDCLASSEX wc;                                              // 'Local var not initialized' [issue 20220106°0711]
   HWND hwnd;
   MSG Msg;

   // Step 1 — Register the Window Class
   wc.cbSize        = sizeof(WNDCLASSEX);
   wc.style         = 0;
   wc.lpfnWndProc   = WndProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = hInstance;
   wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
   wc.lpszMenuName  = NULL;
   wc.lpszClassName = g_szClassName;
   wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

   if ( ! RegisterClassEx(&wc))
   {
      MessageBox ( NULL
                  , "Window Registration Failed!"
                   , "Error!"
                    , MB_ICONEXCLAMATION | MB_OK
                     );
      return 0;
   }

   // Step 2 — Create the Window
   hwnd = CreateWindowEx ( WS_EX_CLIENTEDGE            //
                          , g_szClassName              //
                           , "The title of my window"  //
                            , WS_OVERLAPPEDWINDOW      //
                             , CW_USEDEFAULT           //
                              , CW_USEDEFAULT          //
                               , 480                   //
                                , 240                  //
                                 , NULL                //
                                  , NULL               //
                                   , hInstance         //
                                    , NULL             //
                                     );

   if (hwnd == NULL)
   {
      MessageBox ( NULL
                  , "Window Creation Failed!"
                   , "Error!"
                    , MB_ICONEXCLAMATION | MB_OK
                     );
      return 0;
   }

   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   // Step 3 — The Message Loop (Dispatcher Loop)
   while ( GetMessage(&Msg, NULL, 0, 0) > 0)
   {
      TranslateMessage(&Msg);
      DispatchMessage(&Msg);
   }
   return Msg.wParam;
}
