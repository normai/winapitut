﻿/**
 *  file       : 20220102°1353 winapitut/d05_dlg_one/d05_dlg_one.c
 *  summary    : Displays an About dialog box
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>
#include "d05_resource.h"

const char g_szClassName[] = "myWindowClass";

BOOL CALLBACK AboutDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
   {
      case WM_INITDIALOG:
         return TRUE;
      case WM_COMMAND:
         switch(LOWORD(wParam))
         {
            case IDOK:
               EndDialog(hwnd, IDOK);
               break;
            case IDCANCEL:
               EndDialog(hwnd, IDCANCEL);
               break;
         }
         break;
      default:
         return FALSE;
   }
   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
   {
      case WM_COMMAND:
         switch(LOWORD(wParam))
         {
            case ID_FILE_EXIT:
               PostMessage(hwnd, WM_CLOSE, 0, 0);
               break;
            case ID_HELP_ABOUT:
               {
                  int ret = DialogBox(GetModuleHandle(NULL),
                     MAKEINTRESOURCE(IDD_ABOUT), hwnd, AboutDlgProc);
                  if(ret == IDOK){
                     MessageBox(hwnd, "Dialog exited with IDOK.", "Notice",
                        MB_OK | MB_ICONINFORMATION);
                  }
                  else if(ret == IDCANCEL){
                     MessageBox(hwnd, "Dialog exited with IDCANCEL.", "Notice",
                        MB_OK | MB_ICONINFORMATION);
                  }
                  else if(ret == -1){
                     MessageBox(hwnd, "Dialog failed!", "Error",
                        MB_OK | MB_ICONINFORMATION);
                  }
               }
               break;
         }
         break;
      case WM_CLOSE:
         DestroyWindow(hwnd);
         break;
      case WM_DESTROY:
         PostQuitMessage(0);
         break;
      default:
         return DefWindowProc(hwnd, Message, wParam, lParam);
   }
   return 0;
}


// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInstance
                    , _In_opt_ HINSTANCE hPrevInstance
                     , _In_ LPSTR lpCmdLine
                      , _In_ int nCmdShow
                       )
{
   WNDCLASSEX wc;                                              // 'Local var not initialized' [issue 20220106°0711]
   HWND hwnd;
   MSG Msg;

   wc.cbSize        = sizeof(WNDCLASSEX);
   wc.style         = 0;
   wc.lpfnWndProc   = WndProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = hInstance;
   wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
   wc.lpszMenuName  = MAKEINTRESOURCE(IDR_MYMENU);
   wc.lpszClassName = g_szClassName;
   wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

   if(!RegisterClassEx(&wc))
   {
      MessageBox ( NULL, "Window Registration Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   hwnd = CreateWindowEx(
      WS_EX_CLIENTEDGE,
      g_szClassName,
      "The title of my window",
      WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, CW_USEDEFAULT, 240, 120,
      NULL, NULL, hInstance, NULL);

   if(hwnd == NULL)
   {
      MessageBox ( NULL, "Window Creation Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   while(GetMessage(&Msg, NULL, 0, 0) > 0)
   {
      TranslateMessage(&Msg);
      DispatchMessage(&Msg);
   }
   return Msg.wParam;
}
