﻿// file 20220102°1417  winapitut/d06_dlg_two/d06_resource.h

//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by dlg_two.rc
//
#define IDR_MYMENU                      101
#define IDD_TOOLBAR                     101
#define IDC_PRESS                       1000
#define IDC_OTHER                       1001
#define ID_FILE_EXIT                    40001
#define ID_DIALOG_SHOW                  40002
#define ID_DIALOG_HIDE                  40003

// Next default values for new objects
//
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40004
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
