﻿/**
 *  file       : 20220102°1413 winapitut/d06_dlg_two/d06_dlg_two.c
 *  summary    : Opens a modal window with two buttons which open dialog boxes
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>
#include "d06_resource.h"

const char g_szClassName[] = "myWindowClass";
HWND g_hToolbar = NULL;

BOOL CALLBACK ToolDlgProc ( HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
   {
      case WM_COMMAND:
         switch(LOWORD(wParam))
         {
            case IDC_PRESS:
               MessageBox ( hwnd, "Hi!", "This is a message"
                           , MB_OK | MB_ICONEXCLAMATION
                            );
               break;
            case IDC_OTHER:
               MessageBox ( hwnd, "Bye!", "This is also a message"
                           , MB_OK | MB_ICONEXCLAMATION
                            );
               break;
         }
         break;
      default:
         return FALSE;
   }
   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
   {
      case WM_CREATE:
         g_hToolbar = CreateDialog ( GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_TOOLBAR)
                                    , hwnd, ToolDlgProc
                                     );
         if(g_hToolbar != NULL)
         {
            ShowWindow(g_hToolbar, SW_SHOW);
         }
         else
         {
            MessageBox ( hwnd, "CreateDialog returned NULL", "Warning!"
                        , MB_OK | MB_ICONINFORMATION
                         );
         }
         break;
      case WM_COMMAND:
         switch(LOWORD(wParam))
         {
            case ID_FILE_EXIT:
               PostMessage(hwnd, WM_CLOSE, 0, 0);
               break;
            case ID_DIALOG_SHOW:
               ShowWindow(g_hToolbar, SW_SHOW);
               break;
            case ID_DIALOG_HIDE:
               ShowWindow(g_hToolbar, SW_HIDE);
               break;
         }
         break;
      case WM_CLOSE:
         DestroyWindow(hwnd);
         break;
      case WM_DESTROY:
         DestroyWindow(g_hToolbar);
         PostQuitMessage(0);
         break;
      default:
         return DefWindowProc(hwnd, Message, wParam, lParam);
   }
   return 0;
}

// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInstance
                    , _In_opt_ HINSTANCE hPrevInstance
                     , _In_ LPSTR lpCmdLine
                      , _In_ int nCmdShow
                       )
{
   WNDCLASSEX wc;                                              // 'Local var not initialized' [issue 20220106°0711]
   HWND hwnd;
   MSG Msg;

   wc.cbSize        = sizeof(WNDCLASSEX);
   wc.style         = 0;
   wc.lpfnWndProc   = WndProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = hInstance;
   wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
   wc.lpszMenuName  = MAKEINTRESOURCE(IDR_MYMENU);
   wc.lpszClassName = g_szClassName;
   wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

   if(!RegisterClassEx(&wc))
   {
      MessageBox ( NULL, "Window Registration Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   hwnd = CreateWindowEx(
      WS_EX_CLIENTEDGE,
      g_szClassName,
      "The title of my window",
      WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, CW_USEDEFAULT, 240, 120,
      NULL, NULL, hInstance, NULL);

   if(hwnd == NULL)
   {
      MessageBox ( NULL, "Window Creation Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   while(GetMessage(&Msg, NULL, 0, 0) > 0)
   {
      if(! IsDialogMessage(g_hToolbar, &Msg))
      {
         TranslateMessage(&Msg);
         DispatchMessage(&Msg);
      }
   }
   return Msg.wParam;
}
