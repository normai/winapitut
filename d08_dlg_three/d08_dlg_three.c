﻿/**
 *  file       : 20220102°1433 winapitut/d08_dlg_three/d08_dlg_three.c
 *  summary    : Dialog tricks .. displays white text on black background
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>
#include "d08_resource.h"

HBRUSH g_hbrBackground = NULL;

BOOL CALLBACK DlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
   switch(Message)
   {
      case WM_INITDIALOG:
         g_hbrBackground = CreateSolidBrush(RGB(0, 0, 0));

         SendMessage(hwnd, WM_SETICON, ICON_SMALL, (LPARAM)LoadIcon(NULL,
            MAKEINTRESOURCE(IDI_APPLICATION)));
         SendMessage(hwnd, WM_SETICON, ICON_BIG, (LPARAM)LoadIcon(NULL,
            MAKEINTRESOURCE(IDI_APPLICATION)));
         break;
      case WM_CLOSE:
         EndDialog(hwnd, 0);
         break;
      case WM_CTLCOLORDLG:
         return (LONG)g_hbrBackground;
      case WM_CTLCOLORSTATIC:
         {
            HDC hdcStatic = (HDC)wParam;
            SetTextColor(hdcStatic, RGB(255, 255, 255));
            SetBkMode(hdcStatic, TRANSPARENT);
            return (LONG)g_hbrBackground;
         }
         break;
      case WM_COMMAND:
         switch(LOWORD(wParam))
         {
            case IDOK:
               EndDialog(hwnd, 0);
            break;
         }
         break;
      case WM_DESTROY:
         DeleteObject(g_hbrBackground);
         break;
      default:
         return FALSE;
   }
   return TRUE;
}

// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInstance
                    , _In_opt_ HINSTANCE hPrevInstance
                     , _In_ LPSTR lpCmdLine
                      , _In_ int nCmdShow
                       )
{
   return DialogBox(hInstance, MAKEINTRESOURCE(IDD_MAIN), NULL, DlgProc);
}
