﻿/**
 *  file       : 20220102°1443 winapitut/d09_app_one/d09_app_one.c
 *  summary    : A plain text edit region
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>

const char g_szClassName[] = "myWindowClass";

#define IDC_MAIN_EDIT   101

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_CREATE:
         {
            HFONT hfDefault;
            HWND hEdit;

            hEdit = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", "",
               WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE | ES_AUTOVSCROLL | ES_AUTOHSCROLL,
               0, 0, 100, 100, hwnd, (HMENU)IDC_MAIN_EDIT, GetModuleHandle(NULL), NULL);
            if(hEdit == NULL)
               MessageBox(hwnd, "Could not create edit box.", "Error", MB_OK | MB_ICONERROR);

            hfDefault = (HFONT)GetStockObject(DEFAULT_GUI_FONT);
            SendMessage(hEdit, WM_SETFONT, (WPARAM)hfDefault, MAKELPARAM(FALSE, 0));
         }
         break;
      case WM_SIZE:
         {
            HWND hEdit;
            RECT rcClient;

            GetClientRect(hwnd, &rcClient);

            hEdit = GetDlgItem(hwnd, IDC_MAIN_EDIT);
            SetWindowPos(hEdit, NULL, 0, 0, rcClient.right, rcClient.bottom, SWP_NOZORDER);
         }
         break;
      case WM_CLOSE:
         DestroyWindow(hwnd);
         break;
      case WM_DESTROY:
         PostQuitMessage(0);
         break;
      default:
         return DefWindowProc(hwnd, msg, wParam, lParam);
   }
   return 0;
}

// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInstance
                    , _In_opt_ HINSTANCE hPrevInstance
                     , _In_ LPSTR lpCmdLine
                      , _In_ int nCmdShow
                       )
{
   WNDCLASSEX wc;                                              // 'Local var not initialized' [issue 20220106°0711]
   HWND hwnd;
   MSG Msg;

   wc.cbSize        = sizeof(WNDCLASSEX);
   wc.style         = 0;
   wc.lpfnWndProc   = WndProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = hInstance;
   wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
   wc.lpszMenuName  = NULL;
   wc.lpszClassName = g_szClassName;
   wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

   if(!RegisterClassEx(&wc))
   {
      MessageBox ( NULL, "Window Registration Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   hwnd = CreateWindowEx ( 0 , g_szClassName
                          , "theForger's Tutorial Application"
                           , WS_OVERLAPPEDWINDOW
                            , CW_USEDEFAULT, CW_USEDEFAULT, 480, 320
                             , NULL, NULL, hInstance, NULL
                              );

   if(hwnd == NULL)
   {
      MessageBox ( NULL, "Window Creation Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   while(GetMessage(&Msg, NULL, 0, 0) > 0)
   {
      TranslateMessage(&Msg);
      DispatchMessage(&Msg);
   }
   return Msg.wParam;
}
