﻿<img src="./docs/imgs/20220122o1653.washing-window.v1.x0256y0203.png" align="right" width="212" height="162" data-dims="x0256y0203" style="margin-left:1.7em;" alt="Washing Window">

# CppWinApiTut <span style="padding-left:1.1em; vertical-align:super; font-weight:normal; font-size:66%;">v0.3.0</span>

## Synopsis

- Slogan : Windows API programming demo files series

- Summary : A Visual Studio solution after [www.winprog.org/tutorial/](http://www.winprog.org/tutorial/)
 by Brook Miles. It provides a quick primer into Windows Desktop Application programming in CPP using the Windows API.

- Platform : Windows 10

- Programming language : C++ in Visual Studio

- License : **BSD 3-Clause** (See e.g. [tldrlegal.com/license/bsd-3-clause-license-(revised)](https://tldrlegal.com/license/bsd-3-clause-license-(revised)) )

- Authors : Norbert C. Maier, based on Miles Brooks

- Copyright : Miles Brooks, Norbert C. Maier and contributors

- Status : Applicable

- Missing : ◦ Modernize subtleties like some Windows types ◦ Convert from C to C++

## Details

The projects are:

- **d01\_simple\_window**  — Open a plain window without any functionality <sup><sub><sup>*[file 20220102°1311]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 2 'Basics — A Simple Window'](http://www.winprog.org/tutorial/simple_window.html)

- **d02\_window\_click**   — A plain window, if clicked, it opens a simple dialog <sup><sub><sup>*[file 20220102°1321]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 3 'Basics — A Handling Messages'](http://www.winprog.org/tutorial/window_click.html)

- (Chapter 4 'Basics — The Message Loop' has no demo)

- (Chapter 5 'Basics — Using Resources' has no demo)

- **d03\_menu\_one**       — Adds a menu and a program icon <sup><sub><sup>*[file 20220102°1331]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 6 'Basics — Menus and Icons'](www.winprog.org/tutorial/menus.html)
 • This one has the icon included in the exectuable.

- **d04\_menu\_two**       — Adds a menu and a program icon, the icon is loaded from a file <sup><sub><sup>*[file 20220102°1341]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 6 'Basics — Menus and Icons'](http://www.winprog.org/tutorial/menus.html)
 • This one needs the icon file added to the executables folder

- **d05\_dlg\_one**        — Displays an About dialog box <sup><sub><sup>*[file 20220102°1351]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 7 'Basics — Dialog Boxes'](http://www.winprog.org/tutorial/menus.html)

- **d06\_dlg\_two**        — Opens a modal window with two buttons which open dialog boxes <sup><sub><sup>*[file 20220102°1411]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 8 'Basics — Modeless Dialaogs'](http://www.winprog.org/tutorial/dialogs.html)

- **d07\_ctl\_one**        — Three buttons act on text and list fields <sup><sub><sup>*[file 20220102°1421]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 9 'Basics — Standard Controls'](http://www.winprog.org/tutorial/modeless_dialogs.html)

- **d08\_dlg\_three**      — Dialog tricks .. displays white text on black background <sup><sub><sup>*[file 20220102°1431]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 10 'Basics — Dialog FAQ'](http://www.winprog.org/tutorial/dlgfaq.html)

- **d09\_app\_one**        — A plain text edit region <sup><sub><sup>*[file 20220102°1441]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 'Application — 1. Creating controls at runtime'](http://www.winprog.org/tutorial/app_one.html)

- **d10\_app\_two**       — A simple text file editor <sup><sub><sup>*[file 20220102°1451]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 2.2 'Files and the common dialogs'](http://www.winprog.org/tutorial/app_two.html)

- **d11\_app\_three**     — A simple text file editor <sup><sub><sup>*[file 20220102°1511]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 2.3 'Tool and Status bars'](http://www.winprog.org/tutorial/app_three.html)

- **d12\_app\_four**      — A text editor to open multiple text files <sup><sub><sup>*[file 20220102°1521]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 2.4 'Multiple Document Interface'](http://www.winprog.org/tutorial/app_three.html)

- **d13\_bmp\_one**       — Displays a bitmap <sup><sub><sup>*[file 20220102°1531]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 3.1 'Bitmaps and Device Contexts'](http://www.winprog.org/tutorial/bitmaps.html)

- **d14\_bmp_two**       — Displays a bitmap in multiple transparency modes <sup><sub><sup>*[file 20220102°1541]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 3.2 'Transparency'](http://www.winprog.org/tutorial/transparency.html)

- **d15\_anim\_one**      — A ball moves over the window <sup><sub><sup>*[file 20220102°1551]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 3.3 'Timers and Animation'](http://www.winprog.org/tutorial/animation.html)


- **d16\_font\_one**      — Displays the window size in various fonts and background colors <sup><sub><sup>*[file 20220102°1611]*</sup></sub></sup>
 <br>Tutorial text see
 [Chapter 3.4 'Text, Fonts and Colours'](http://www.winprog.org/tutorial/fonts.html)

- **d17\_test**          — Just shows a dialog box with OK button <sup><sub><sup>*[file 20220102°1621]*</sup></sub></sup>

- Chapter 4.1 'References'
 [www.winprog.org/tutorial/references.html](http://www.winprog.org/tutorial/references.html)

- Chapter 4.2 'Free Visual C++'
 [www.winprog.org/tutorial/msvc.html](http://www.winprog.org/tutorial/msvc.html)

- Chapter 5.A 'Solution to Common Errors'
 [www.winprog.org/tutorial/errors.html](http://www.winprog.org/tutorial/errors.html)

- Chapter 5.B 'API vs. MFC'
 [www.winprog.org/tutorial/apivsmfc.html](http://www.winprog.org/tutorial/apivsmfc.html)

- Chapter 5.C 'Resource file notes'
 [www.winprog.org/tutorial/resnotes.html](http://www.winprog.org/tutorial/resnotes.html)

## Credits

The original tutorial by Miles Brook is found on
 [www.winprog.org/tutorial/](http://www.winprog.org/tutorial/).
 The page and it's subchapters are linked abundandly above.
 The download file was `forgers-win32-tutorial-source.zip` (70 680 bytes)

The 'Window Washing' logo file on top right comes from
 [openclipart.org/detail/301205/washing-window](https://openclipart.org/detail/301205/washing-window)
 under the [Creative Commons Zero 1.0 Public Domain License](http://creativecommons.org/publicdomain/zero/1.0/)

## Further reading

An overview on my projects is found on
 [github.com/normai](https://github.com/normai)

&nbsp;

Norbert, 2022-Jan-23

<sup><sub><sup>*[file 20211229°1712]* ܀Ω</sup></sub></sup>
