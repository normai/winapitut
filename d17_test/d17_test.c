﻿/**
 *  file       : 20220102°1623 winapitut/d17_test/d17_test.c
 *  summary    : Just shows a dialog box with OK button
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>

// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInst
                    , _In_opt_ HINSTANCE hPrev
                     , _In_ LPSTR pszCmdLine
                      , int iCmdShow
                       )
{
   MessageBox(NULL, "Narf!", "Pinky says...", MB_OK | MB_ICONEXCLAMATION);
   return 0;
}
