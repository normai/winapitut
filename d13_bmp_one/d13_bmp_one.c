﻿/**
 *  file       : 20220102°1533 winapitut/d13_bmp_one/d13_bmp_one.c
 *  summary    : Displays a bitmap
 *  copyright  : © 1998 - 2022 Miles Brook
 *  copyright  : © 2022 Norbert C. Maier and contributors
 *  license    : BSD 3-Clause (choosealicense.com/licenses/bsd-3-clause/)
 *  authors    : Miles Brook, Norbert C. Maier
 *  encoding   : UTF-8-with-BOM
 */
#include <windows.h>
#include "d13_resource.h"

const char g_szClassName[] = "myWindowClass";

HBITMAP g_hbmBall = NULL;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg)
   {
      case WM_CREATE:
         g_hbmBall = LoadBitmap(GetModuleHandle(NULL), MAKEINTRESOURCE(IDB_BALL));
         if(g_hbmBall == NULL)
            MessageBox(hwnd, "Could not load IDB_BALL!", "Error", MB_OK | MB_ICONEXCLAMATION);
         break;
      case WM_CLOSE:
         DestroyWindow(hwnd);
         break;
      case WM_PAINT:
      {
         // Just a note, never use a MessageBox from inside WM_PAINT
         // The box will cause more WM_PAINT messages and you'll probably end up
         // stuck in a loop

         BITMAP bm;
         PAINTSTRUCT ps;

         HDC hdc = BeginPaint(hwnd, &ps);

         HDC hdcMem = CreateCompatibleDC(hdc);
         HBITMAP hbmOld = (HBITMAP)SelectObject(hdcMem, g_hbmBall);

         GetObject(g_hbmBall, sizeof(bm), &bm);

         BitBlt(hdc, 0, 0, bm.bmWidth, bm.bmHeight, hdcMem, 0, 0, SRCCOPY);

         SelectObject(hdcMem, hbmOld);
         DeleteDC(hdcMem);

         EndPaint(hwnd, &ps);
      }
      break;

      case WM_DESTROY:
         DeleteObject(g_hbmBall);
         PostQuitMessage(0);
         break;

      default:
         return DefWindowProc(hwnd, msg, wParam, lParam);
   }
   return 0;
}

// Supplementing annotations solves issue 20220106°0721 [chg 20220106°0727]
int WINAPI WinMain ( _In_ HINSTANCE hInstance
                    , _In_opt_ HINSTANCE hPrevInstance
                     , _In_ LPSTR lpCmdLine
                      , _In_ int nCmdShow
                       )
{
   WNDCLASSEX wc;                                              // 'Local var not initialized' [issue 20220106°0711]
   HWND hwnd;
   MSG Msg;

   wc.cbSize        = sizeof(WNDCLASSEX);
   wc.style         = 0;
   wc.lpfnWndProc   = WndProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = hInstance;
   wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
   wc.lpszMenuName  = NULL;
   wc.lpszClassName = g_szClassName;
   wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

   if(!RegisterClassEx(&wc))
   {
      MessageBox ( NULL, "Window Registration Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   hwnd = CreateWindowEx ( WS_EX_CLIENTEDGE
                         , g_szClassName
                         , "A Bitmap Program"
                         , WS_OVERLAPPEDWINDOW
                         , CW_USEDEFAULT, CW_USEDEFAULT, 240, 120
                         , NULL, NULL, hInstance, NULL
                         );

   if(hwnd == NULL)
   {
      MessageBox ( NULL, "Window Creation Failed!", "Error!"
                  , MB_ICONEXCLAMATION | MB_OK
                   );
      return 0;
   }

   ShowWindow(hwnd, nCmdShow);
   UpdateWindow(hwnd);

   while(GetMessage(&Msg, NULL, 0, 0) > 0)
   {
      TranslateMessage(&Msg);
      DispatchMessage(&Msg);
   }
   return Msg.wParam;
}
